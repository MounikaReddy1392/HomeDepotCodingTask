//
//  Consatnts.swift
//  HomeDepotCodingTask
//
//  Created by Mounika Gouni on 3/17/18.
//  Copyright © 2018 sofistel. All rights reserved.
//


import UIKit
import Foundation

//MARK :- Constants
struct WebservicesURL {
  static let baseURL = "https://api.github.com/users/"
    
}

struct CollectionCellIdentifier {
    static let gitRepoCollectioncellIdentifier = "GitRepoListCollectionViewCell"
}

struct AlertMessage {
    static let userEnterValue = "Please enter text either as facebook or apple"
     static let sometingWentWrong = "Something Went wrong. Please try agian."
    static let emptyStringAlert = "Please enter value in textfield"
}

struct HomeDepotConstants {
    static let apple = "apple"
    static let facebook = "facebook"
    static let alertTitle = "Alert"
}

