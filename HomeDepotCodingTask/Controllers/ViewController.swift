//
//  ViewController.swift
//  HomeDepotCodingTask
//
//  Created by Mounika Gouni on 3/17/18.
//  Copyright © 2018 sofistel. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var gridListSengmentControl: UISegmentedControl!
    @IBOutlet weak var actIndBgView: UIView!
    @IBOutlet weak var actindicator: UIActivityIndicatorView!
    @IBOutlet weak var gitRepoCollectionView: UICollectionView!
    let gitRepoViewModel = GitRepoViewModel()
    var pageNo = Int()
    var orgType = String()
    let inset: CGFloat = 5
    let minimumLineSpacing: CGFloat = 5
    let minimumInteritemSpacing: CGFloat = 5
    var cellsPerRow = Int()
    
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home Depot Coding Task"
        hideActivityIndicator()
        cellsPerRow = 2
        pageNo = 1
       gitRepoCollectionView?.contentInsetAdjustmentBehavior = .always
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.inputTextField.becomeFirstResponder()
    }
    
    //MARK: - Custom Methods
    
    func getDataFromServer()  {
        showActivityIndicator()
        gitRepoViewModel.getDataFromServer(String(pageNo),_orgType: orgType, parentViewController: self){ (success) in
            DispatchQueue.main.async {
                self.hideActivityIndicator()
            }
            if success {
                DispatchQueue.main.async {
                    self.gitRepoCollectionView.reloadData()
                }
            }
            else{
                let message  = Utility.createAlertWithoutAction(message: AlertMessage.sometingWentWrong, title: "Alert")
                self.present(message, animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - TextField Delegate Methods
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        let trimmedString = textField.text?.trimmingCharacters(in: .whitespaces)
        if trimmedString?.lowercased() == HomeDepotConstants.apple || trimmedString?.lowercased() == HomeDepotConstants.facebook {
            orgType = trimmedString!
            getDataFromServer()
            
        }
        else{
            if (trimmedString?.isEmpty)! {
                let message  = Utility.createAlertWithoutAction(message: AlertMessage.emptyStringAlert, title: HomeDepotConstants.alertTitle)
                self.present(message, animated: true, completion: nil)
            }
            else{
                let message  = Utility.createAlertWithoutAction(message: AlertMessage.userEnterValue, title: HomeDepotConstants.alertTitle)
                self.present(message, animated: true, completion: nil)
            }
            
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - ActInd Custom Methods
    
    func showActivityIndicator()  {
        actIndBgView.isHidden = false
        actindicator.startAnimating()
    }
    func hideActivityIndicator()  {
        actIndBgView.isHidden = true
        actindicator.stopAnimating()
    }
    
    //MARK: - Action Methods
    
    @IBAction func gridListSengmentControlAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            cellsPerRow = 2
            gitRepoCollectionView.reloadData()
        }
        else{
            cellsPerRow = 1
            gitRepoCollectionView.reloadData()
        }
    }

}

//MARK: - ViewController Extendion

//MARK: - UICollection View Extension


extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gitRepoViewModel.gitRepoModelArray.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let marginsAndInsets = inset * 2 + collectionView.safeAreaInsets.left + collectionView.safeAreaInsets.right + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        if cellsPerRow == 1 {
            return CGSize(width: itemWidth, height: 180)
        }
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return minimumLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return minimumInteritemSpacing
    }
    
    
    func collectionView(_ collectionView: UICollectionView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let  gitRepoCell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionCellIdentifier.gitRepoCollectioncellIdentifier, for: indexPath) as? GitRepoCollectionViewCell {
            gitRepoCell.contentView.layer.borderWidth = 1
            gitRepoCell.contentView.layer.borderColor = UIColor.black.cgColor
            
            let gitRepoObj =  gitRepoViewModel.gitRepoModelArray[indexPath.row]
            if let createdDate = gitRepoObj.createdAt {
                gitRepoCell.createdDate.text! = createdDate
            }
            
            if let description = gitRepoObj.descriptionField {
                gitRepoCell.descriptionLabel.text! = description
            }
            
            if let  licenceValue = gitRepoObj.license?.name {
                gitRepoCell.licenseLabel.text! = licenceValue
            }
            if let repoName = gitRepoObj.name {
                gitRepoCell.nameLabel.text! = repoName
            }
            
            
            return gitRepoCell
            
        }
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == gitRepoViewModel.gitRepoModelArray.count - 1 {
            pageNo = pageNo + 1
            getDataFromServer()
        }
    }
    
    
}

