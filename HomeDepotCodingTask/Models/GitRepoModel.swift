//
//  GitHubAPI.swift
//  HomeDepotCodingTask
//
//  Created by Mounika Gouni on 3/16/18.
//  Copyright © 2018 Mounika Gouni. All rights reserved.
//

import UIKit


//MARK: - Data from - https://api.github.com/users/apple/repos? page=1&per_page=10 
struct APIData : Codable {
    let name : String?
    let createdAt : String?
    let descriptionField : String?
    let license : License?
    
    enum CodingKeys: String, CodingKey {
        
        case createdAt = "created_at"
        case license
        case descriptionField = "description"
        case name = "name"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        license = try License(from: decoder)
        descriptionField = try values.decodeIfPresent(String.self, forKey: .descriptionField)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
}
struct License : Codable {
    let name : String?
    enum CodingKeys: String, CodingKey {
        case name = "name"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        
    }
    
}


