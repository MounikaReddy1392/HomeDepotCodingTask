//
//  Utility.swift
//  HomeDepotCodingTask
//
//  Created by Mounika Gouni on 3/17/18.
//  Copyright © 2018 sofistel. All rights reserved.
//

import UIKit

class Utility: NSObject {

    static func createAlertWithoutAction(message: String, title: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle:.alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        alert.addAction(okAction)
        return alert
    }
    
}
