//
//  GitRepoListCollectionViewCell.swift
//  HomeDepotCodingTask
//
//  Created by Mounika Gouni on 3/17/18.
//  Copyright © 2018 sofistel. All rights reserved.
//

import UIKit

class GitRepoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var createdDate: UILabel!
    @IBOutlet weak var licenseLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
