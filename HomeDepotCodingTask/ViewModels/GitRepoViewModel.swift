//
//  GitRepoViewModel.swift
//  HomeDepotCodingTask
//
//  Created by Mounika Gouni on 3/17/18.
//  Copyright © 2018 sofistel. All rights reserved.
//

import UIKit

class GitRepoViewModel: NSObject {
    
    var gitRepoModelArray = [APIData]()
    
    //This method attempts to get data depending on the user input apple/facebook
    public func getDataFromServer(_ pageNo : String , _orgType : String , parentViewController: UIViewController, completionBlock:@escaping (Bool) -> ()) {
        
        let URLString = WebservicesURL.baseURL + "\(_orgType)/repos?" + "page=\(pageNo)&per_page=10"
        let url = URL(string: URLString)
        let request = URLRequest(url: url!)
        //Created URLSession with default configuration
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) -> Void in
            if let data = data {
                if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                    do {
                    let decoder = JSONDecoder()
                    self.gitRepoModelArray.append(contentsOf: try decoder.decode([APIData].self, from: data))
                    } catch {
                        print(error)
                    }
                    completionBlock(true)
                    
                } else {
                    completionBlock(false)
                }
            }
        })
        task.resume()
    }
    }


